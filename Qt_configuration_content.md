## QT6.2.4环境配置
**步骤如下：**
1. 首先，搜索“清华镜像帮助QT”，得到如下所示的页面
   ![](Tsinghua_mirror_help_QT.png)

2. 然后，点击第一个"qt|镜像站使用帮助"
   ![](Qt_software_repository_mirror_usagehelp.png)

3. 复制链接下载在线安装器，并使用浏览器搜索，如下图
   ![](Tsinghua%20mirror_installers_index.png)
   附连接：<https://mirrors.tuna.tsinghua.edu.cn/qt/official_releases/online_installers/>

4. windows系统使用.exe结尾。【注：.run——linux; .dmg——mac】
，下载完成
   ![](windows_sys_file_open.png)

5. 点击运行之后，注册账户 ，并登录
   ![](Qt_register&signup.png)


6. 勾选协议，两个都勾选，点击下一步
   ![](Open_source_obligation.png)


7. 选择不发送反馈，点击下一步
   ![](Contribute_to_Qt_Development.png)

8. 选择自定义配置，点击下一步
   ![](custom_installation.png)


9. 选择组件，如下所示，点击下一步
   ![](choose_suite.png)


10. 协议选择默认Cmake协议
   ![](Choose_Cmake.png)
**等待下载组件以及安装，30min左右.....**


1. 勾选如下，点击完成
![](Finish_inatallation.png)
**安装完成**
